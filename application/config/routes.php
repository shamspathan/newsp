<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'news';
$route['404_override'] = '';
$route['translate_uri_dashes'] = true;
//$route['admin/ajax/(:any)/(:any)/(:any)'] = 'admin/ajax/$2/$3';

$route['user/profile'] = 'user/profile';
$route['dbgen'] = 'test/dbgen';
$route['dbload'] = 'test/loadNews';
$route['dbrel'] = 'test/dbrel';
$route['guard'] = 'guard';
$route['guard/(:any)'] = 'guard/$1';
$route['backdesk'] = 'backDesk';
$route['dashboard'] = 'Dashboard';
$route['ajax'] = 'Ajax';
$route['ajax/(:any)'] = 'Ajax/$1';
$route['ajax/(:any)/(:any)'] = 'Ajax/$1/$2';
$route['dashboard/(:any)'] = 'Dashboard/$1';
$route['test'] = 'test';
$route['test/(:any)'] = 'test/$1';
$route['user'] = 'user';
$route['user/(:any)'] = 'user/$1';
$route['admin'] = 'admin/login';
$route['admin/index'] = 'admin/index';
$route['admin/(:any)'] = 'admin/$1';
$route['index'] = 'news/index';
$route['comment'] = 'article/userComment';
$route['updateComment'] = 'article/updateComment';
$route['article/(:any)'] = 'article/$1';
$route['article/(:any)/(:any)'] = 'article/$1/$2';
$route['(:any)'] = 'news/route/$1';
$route['(:any)/(:any)'] = 'news/route/$1/$2';
$route['(:any)/(:any)/(:any)'] = 'news/route/$1/$2/$3';
$route['(:any)/(:any)/article/(:any)'] = 'news/article/$1/$2/$3';

//$route['(:any)/(:any)'] = 'news/$1/$2';
//$route['(:any)/(:any)'] = 'news/$1'; //goto error page
