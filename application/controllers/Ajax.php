<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ajax extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
         $this->load->model('AjaxModel', 'ajax');
         $this->load->model('AboutNews', 'newsdb');
    }

    public function getsub($forMenuId = 1)
    {
        $submenues = $this->ajax->getSubmenues($forMenuId);
        $json = json_encode($submenues);
        echo $json;
    }

    public function getnews($catid, $subcatid)
    {
        //$json         = json_encode("got cat:".$cat."and sub:".$sub);
        $catname = $this->newsdb->getCatname($catid);
        $subcatname = $this->newsdb->getSubcatname($catid, $subcatid);
        $tableName = $catname . "_" . $subcatname;
        $contents = json_encode($this->ajax->getnewsup($tableName));
        echo $contents;
    }

    public function pubnews($catid, $subcatid, $nid)
    {

        $catname = $this->newsdb->getCatname($catid);
        $subcatname = $this->newsdb->getSubcatname($catid, $subcatid);
        $tableName = $catname . "_" . $subcatname;
        $success = json_encode($this->ajax->pubnews($tableName, $nid));
        echo $success;
    }

}
