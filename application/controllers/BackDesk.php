<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BackDesk extends CI_Controller
{
    /**
    Variable from construct
    $this->session->company
    $this->session->menues
     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('aboutNews', 'newsdb');
        $this->load->helper('my_helper');

        if (!isset($this->session->company)) {
            $this->session->company = $this->newsdb->getCompanyInfo();
        }

        if (!isset($this->session->menues)) {
            $this->session->menues = $this->newsdb->getCats();
        }

        if (!isset($this->session->submenues)) {
            $this->session->submenues = $this->newsdb->getSubCats();
        }
    }

    public function index(){
        $this->load->view('admin/index');
    }
}