<?php

defined('BASEPATH') OR exit('No direct script access allowed');
define('DIR', 'admin/');


class Dashboard extends CI_Controller
{
    
    var $tblName1;
    var $tblName2;
    var $tblName3;
    var $menues;
    var $submenues;
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('aboutNews', 'newsdb');
        $this->load->helper('my_helper');

        if (!isset($this->session->company)) {
            $this->session->company = $this->newsdb->getCompanyInfo();
        }

        if (!isset($this->session->menues)) {
            $this->session->menues = $this->newsdb->getCats();
        }

        if (!isset($this->session->submenues)) {
            $this->session->submenues = $this->newsdb->getSubCats();
        }        
    }
    
    function index()
    {   //echo "hello wordl";exit;
        $data['greeting'] = "hello world";
        $this->load->view(DIR.'index', $data);
    }
    
    public function post_news(){
        $data['message'] = null ;

        // যদি নিউজ সাবমিট করা হয়
        if(array_key_exists("post",$this->input->post())){
            // ইনপুট সেনিটাইজ 
            $post['data']['cat_id'] = sanitizeSting($this->input->post('catid'));
            $post['data']['subcat_id'] = sanitizeSting($this->input->post('subid'));
            $post['data']['title'] = sanitizeSting($this->input->post('title'));
            $post['data']['content'] = sanitizeSting($this->input->post('content'));
            $post['data']['image'] = sanitizeSting($this->input->post('image'));
            //print_r($_FILES);exit;           
            $post['data']['image'] = file_get_contents($_FILES['image']['tmp_name']);
            //print_r($post['image']);exit;     
            // আইডি থেকে কেটাগরী নাম সেভ করি
            foreach($this->session->menues as $cat){
                if($cat['catID'] == $post['data']['cat_id']){
                    $post['catName'] = $cat['catEN'];                    
                }
            }
            // আইডি থেকে সাবক্যাটাগরীর নাম সেভ করি
            foreach($this->session->submenues[$post['data']['cat_id']] as $sub){
                if($sub['id'] == $post['data']['subcat_id']){
                    $post['subName'] = $sub['subcatEN'];                    
                }
            }
            // পোস্টদাতার আইডি সেভ user field
            if($_SESSION['userInfo']['id']){
                $post['data']['user'] = $_SESSION['userInfo']['id'];
            };
            // পোস্ট
                $post = $this->newsdb->post_news($post);
                if($post = 'success'){
                    $data['message']['success'] = "News posted";
                } else {
                    $data['message']['error'] =  "Sorry, News not posted for some error";
                }
        } 
        
        $this->load->view(DIR.'post_news', $data);
    }
    function nbc()
    {
        //news by cat
        $data['menues']     = $this->menues;
        $data['submenues']    = $this->submenues;
        $data['userName']     = $this->session->userdata('username');
        $data['contentTitle'] = "News by Catagories";
        $data['nbc']         = true ; //for class activation in sidebar
        $data['seg1']           = $this->load->view(DIR."upperPart",    $data, true);
        $data['seg2']         = $this->load->view(DIR."sideMenu",        $data, true);
        $data['seg3']         = $this->load->view(DIR."contentHeader", $data, true);
        $data['seg4']         = $this->load->view(DIR."nbcDetailed",    $data, true);
        $data['seg5']         = $this->load->view(DIR."bottomPart",    $data, true);
        $this->load->view(DIR."index", $data);
    }
    
    function nifp()
    {
        //news in front page
        $data['userName']     = $this->session->userdata('username');
        $data['nifp']         = true;
        $data['contentTitle'] = "Frontpage News";
        $data['seg1']         = $this->load->view(DIR."upperPart", $data, true);
        $data['seg2']         = $this->load->view(DIR."sideMenu", $data, true);    
        $data['seg3']         = $this->load->view(DIR."contentHeader", $data, true);
        $data['seg4']         = $this->load->view(DIR."bottomPart", $data, true);
        $this->load->view(DIR."index", $data);        
    }
    
    function userList($userType='')
    {
        //user management	
        $data['userName']     = $this->session->userdata('username');
        $data['userList']     = true;
        $data['contentTitle'] = "User List";
        $data['users']        = $this->user_model->get_user_list($userType);
        $data['seg1']         = $this->load->view(DIR."upperPart", $data, true);
        $data['seg2']         = $this->load->view(DIR."sideMenu", $data, true);
        $data['seg3']         = $this->load->view(DIR."contentHeader", $data, true);
        $data['seg4']         = $this->load->view(DIR."userList", $data, true);        
        $data['seg5']         = $this->load->view(DIR."bottomPart", $data, true);
        $this->load->view(DIR."index", $data);
    }

    function postNews()
    {
        //user management	
        $data['menues']     = $this->menues;
        $data['submenues']     = $this->submenues;
        $data['userName']     = $this->session->userdata('username');
        $data['userList']     = true;
        $data['contentTitle'] = "Post News";
        $data['seg1']         = $this->load->view(DIR."upperPart", $data, true);
        $data['seg2']         = $this->load->view(DIR."sideMenu", $data, true);
        $data['seg3']         = $this->load->view(DIR."contentHeader", $data, true);
        $data['seg4']         = $this->load->view(DIR."newsPost", $data, true);
        $data['seg5']         = $this->load->view(DIR."bottomPart", $data, true);
        $this->load->view(DIR."index", $data);
    }

    
}
