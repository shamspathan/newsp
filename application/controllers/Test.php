<?php

class Test extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('aboutNews', 'newsdb');
		if (!isset($this->session->menues)) {
			$this->session->menues = $this->newsdb->getMenues();
		}
  }

  function dbGen() {

    $this->_createTableCat();
    $this->_createTableSubcat();
    $this->_createTableCompany();
    $this->_createTableComment();
    $this->_createTablePerson();
    $this->_createTableUser();
    $this->_createTableFooter();
    $this->loadData();
}

private function loadData() {
    $this->loadCompany();
    $this->loadUser();
}

public function loadNews() {
    $newsItemNumber = 13;
    $this->createNewsTable();
    $this->loadTableData($newsItemNumber);
}

private function loadUser() {
    $sql = "INSERT INTO `users_list` (`id`, `email`, `pass`, `type`, `active`, `soft_delete`, `hash`)
    VALUES ('1', 'demo@demo.com', sha1('demo'), '1', CONV('1', 2, 10) + 0, CONV('0', 2, 10) + 0, '0');";
    $this->db->query($sql);
    $sql = "INSERT INTO `persons` (`id`, `fname`, `email`, `lname`, `nick`, `phon`, `address`, `country`, `location`, `locality`, `profile`, `company`, `dob`, `nid`, `image`, `created_at`, `updated_at`)
    VALUES ('1', 'demo', 'demo@demo.com', 'user', 'demo user', '01770075589', 'Dhaka', 'bangladesh', 'Dhaka', NULL, 'nothing', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL);";
    $this->db->query($sql);

}

private function loadCompany() {
    $sql = "INSERT INTO `company` (`name`, `des`, `copyright`, `editor`, `address`, `phon`, `fax`, `email`)
    VALUES ('খবর প্রতিদিন', 'এটি একটি পরীক্ষামূলক সংবাদ সরবরাহ অন্তর্জাল সেবা।', '1995-2018', 'শামশ সুজন', 'ঢাকা', '+৮৮০ ১৭৭০০৭৫৫৮৯', '১২৩৪৫৬৭৮', 'suspathan@gmail.com');";
    $this->db->query($sql);
}

private function _createTableSubcat() {

    $sql = "CREATE TABLE `subcatagories` (
    `id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
    `subcatEN` varchar(100) NOT NULL,
    `subcatBN` varchar(100) NOT NULL,
    `catID` smallint(3) unsigned NOT NULL,
    PRIMARY KEY (`id`)
    )
    ENGINE=InnoDB";
    $this->db->query($sql);

    $sql = "ALTER TABLE `subcatagories`
    ADD INDEX `catID` (`catID`)";
    $this->db->query($sql);    

    $sql = "INSERT INTO `subcatagories` (`id`, `subcatEN`, `subcatBN`, `catID`) VALUES
    (1, 'divisions',    'বিভাগ',    1),
    (2, 'politics', 'রাজনীতি',  1),
    (3, 'government',   'সরকার',    1),
    (4, 'crime',    'অপরাধ',    1),
    (5, 'justice',  'আইন ও বিচার',  1),
    (6, 'environment',  'পরিবেশ',   1),
    (7, 'accident', 'দুর্ঘটনা', 1),
    (8, 'parliement',   'সংসদ', 1),
    (9, 'capitalcity',  'রাজধানী',  1),
    (10,    'population',   'জনসংখ্যা', 1),
    (11,    'others',   'বিবিধ',    1),
    (12,    'usa',  'যুক্তরাস্ট্র', 2),
    (13,    'uk',   'যুক্তরাজ্য',   2),
    (14,    'india',    'ভারত', 2),
    (15,    'pakistan', 'পাকিস্তান',    2),
    (16,    'asia', 'এশিয়া',    2),
    (17,    'europe',   'ইউরোপ',    2),
    (18,    'africa',   'আফ্রিকা',  2),
    (19,    'arabworld',    'আরব বিশ্ব',    2),
    (20,    'latinamerica', 'লাতিন আমেরিকা',    2),
    (21,    'unitednations',    'জাতিসংঘ',  2),
    (22,    'others',   'বিভিদ',    2),
    (23,    'stocksmarket', 'শেয়র বাজার',   3),
    (24,    'business', 'বানিজ্য সংবাদ',    3),
    (25,    'garments', 'পোশাক শিল্প',  3),
    (26,    'budget',   'বাজেট',    3),
    (27,    'humanresource',    'মানবসম্পদ',    3),
    (28,    'crime',    'অপরাধ',    3),
    (29,    'analysis', 'বিশ্লেষন', 3),
    (30,    'others',   'বিবিধ',    3),
    (31,    'international',    'বিদেশের খবর',  3),
    (32,    'editorial',    'সম্পাদকীয়',    4),
    (33,    'politics', 'রাজনীতি',  4),
    (34,    'economy',  'অর্থনীতি', 4),
    (35,    'religion', 'ধর্ম', 4),
    (36,    'international',    'আন্তর্জাতিক',  4),
    (37,    'education',    'শিক্ষা',   4),
    (38,    'interview',    'সাক্ষাৎকার',   4),
    (39,    'debate',   'বিতর্ক',   4),
    (40,    'letter',   'চিঠিপত্র', 4),
    (41,    'others',   'বিবিধ',    4),
    (42,    'cricket',  'ক্রিকেট',  5),
    (43,    'football', 'ফুটবল',    5),
    (44,    'tenis',    'টেনিস',    5),
    (45,    'interview',    'সাক্ষাৎকার',   5),
    (46,    'athletics',    'অ্যাথলেটিক্স', 5),
    (47,    'others',   'বিবিধ',    5),
    (48,    'livescore',    'লাইভ স্কোর',   5),
    (49,    'champions',    'চ্যাম্পিয়ন্স ট্রফি',   5),
    (58,    'naksha',   'নকশা', 6),
    (59,    'adhuna',   'অধুনা',    6),
    (60,    'shopnoniye',   'স্বপ্ন নিয়ে',  6),
    (61,    'anondo',   'আনন্দ',    6),
    (62,    'chutirdine',   'ছুটির দিনে',   6),
    (63,    'femalestage',  'নারী মন্ঞ',    6),
    (64,    'roshalo',  'রস+আলো',   6),
    (65,    'gollachot',    'গোল্লা ছোট',   6),
    (66,    'shilpoosahityo',   'শিল্প ও সাহিত্য',  6),
    (67,    'projonmodotcom',   'প্রজন্ম ডট কম',    6),
    (68,    'amarctg',  'আমার চট্টগ্রাম',   6),
    (69,    'bondhushova',  'বন্ধুসভা', 6),
    (70,    'fashion',  'ফ্যাশন',   7),
    (71,    'style',    'স্টাইল',   7),
    (72,    'beautification',   'রুপচর্চা', 7),
    (73,    'travel',   'বেড়ানো',   7),
    (74,    'relation', 'সম্পর্ক',  7),
    (75,    'occupation',   'পেশা', 7),
    (76,    'advice',   'পরামর্শ',  7),
    (77,    'doctor',   'আমার ডাক্তার', 7),
    (78,    'homedecoration',   'গৃহ সজ্জা',    7),
    (79,    'jenenin',  'জেনে নিন', 7),
    (80,    'food', 'খাবার দাবার',  7),
    (81,    'horoscope',    'রাশিফল',   7),
    (82,    'others',   'বিবিধ',    7),
    (83,    'ekjholok', 'এক ঝলক',   8),
    (84,    'chobirgolpo',  'ছবির গল্প',    8),
    (85,    'bangladesh',   'বাংলাদেশ', 8),
    (86,    'fashion',  'ফ্যাশন',   8),
    (87,    'sports',   'খেলা', 8),
    (88,    'entertainment',    'বিনোদন',   8),
    (89,    'international',    'আন্তর্জাতিক',  8),
    (90,    'science',  'বিজ্ঞান ও প্রযুক্তি',  8),
    (91,    'bitorko',  'বিতর্ক উৎসব',  8),
    (92,    'entertainment',    'বিনোদন',   9),
    (93,    'sports',   'খেলা', 9),
    (94,    'science',  'বিজ্ঞান ও প্রযুক্তি',  9),
    (95,    'bangladesh',   'বাংলাদেশ', 9),
    (96,    'international',    'আন্তর্জাতিক',  9),
    (98,    'dhaliwoodfilm',    'ঢালিউড',   10),
    (99,    'bollywoodfilm',    'বলিউড',    10),
    (100,   'hollywoodfilm',    'হলিউড',    10),
    (101,   'alapon',   'আলাপন',    10),
    (102,   'stageperformance', 'মঞ্চ', 10),
    (103,   'music',    'সংগীত',    10),
    (104,   'television',   'টেলিভিশন', 10),
    (105,   'others',   'বিবিধ',    10),
    (106,   'computer', 'কম্পিউটার',    11),
    (107,   'mobilephone',  'মোবাইল ফোন',   11),
    (108,   'automobile',   'অটোমোবাইল',    11),
    (109,   'research', 'গবেষণা',   11),
    (110,   'mohakash', 'মহাকাশ',   11),
    (111,   'stars',    'তারকা',    11),
    (112,   'games',    'গেমস', 11),
    (113,   'freelancing',  'ফ্রিল্যান্সিং',    11),
    (114,   'review',   'রিভিউ',    11),
    (115,   'advice',   'বিজ্ঞান ও প্রযুক্তি ', 11),
    (116,   'multimedia',   'মাল্টিমিডিয়া',    11),
    (117,   'news', 'খবরাখবর',  11),
    (118,   'others',   'বিবিধ',    11),
    (119,   'femalestage',  'নারী', 12),
    (120,   'personality',  'ব্যক্তিত্ব',   12),
    (121,   'torun',    'তরুণ', 12),
    (122,   'others',   'বিবিধ',    14),
    (123,   'poem', 'কবিতা',    13),
    (124,   'shortstories', 'ছোটগল্প',  13),
    (125,   'industrialart',    'শিল্পকলা', 13),
    (126,   'interview',    'সাক্ষাৎকার',   13),
    (127,   'treatise', 'নিবন্ধ',   13),
    (128,   'translation',  'অনুবাদ',   13),
    (129,   'childrensliterature',  'শিশু সাহিত্য', 13),
    (130,   'bookdiscussion',   'বইয়ের আলোচনা',    13),
    (131,   'others',   'বিবিধ',    13),
    (132,   'meritorious',  'মেধাবী',   14),
    (133,   'institution',  'শিক্ষাঙ্গন',   14),
    (134,   'preparation',  'প্রস্তুতি নিন',    14),
    (135,   'jenerakhun',   'জেনে রাখুন',   14),
    (136,   'primaryandsecondaryeducation', 'প্রাথমিক ও মাধ্যমিক শিক্ষা',   14),
    (137,   'highersecondaryeducation', 'উচ্চ মাধ্যমিক শিক্ষা', 14),
    (138,   'highereducation',  'উচ্চশিক্ষা',   14),
    (139,   'others',   'বিবিধ',    12);";
    $this->db->query($sql);
}

private function _createTableCat() {
    $sql = "CREATE TABLE `catagories` (
    `catID` SMALLINT(3) unsigned NOT NULL AUTO_INCREMENT,
    `catEN` VARCHAR(100)  NOT NULL,
    `catBN` VARCHAR(100)  NOT NULL,
    PRIMARY KEY (`catID`)
    )
    ENGINE=InnoDB COMMENT='Will store all catagories for main menu'";
    $this->db->query($sql);

    $sql = "INSERT INTO `catagories` (`catID`, `catEN`, `catBN`) VALUES
    (1, 'bangladesh',   'বাংলাদেশ'),
    (2, 'international',    'আন্তর্জাতিক'),
    (3, 'economy',  'অর্থনীতি'),
    (4, 'opinion',  'মতামত'),
    (5, 'sports',   'খেলা'),
    (6, 'feature',  'ফিচার'),
    (7, 'lifestyle',    'জীবনযাপন'),
    (8, 'photo',    'ছবি'),
    (9, 'video',    'ভিডিও'),
    (10,    'entertainment',    'বিনোদন'),
    (11,    'technology',   'বিজ্ঞান ও প্রযুক্তি'),
    (12,    'weare',    'আমরা'),
    (13,    'artandliterature', 'শিল্প ও সাহিত্য'),
    (14,    'education',    'শিক্ষা')";
    $this->db->query($sql);
}

private function _createTableUser() {

    $sql = "CREATE TABLE `users_list` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `email` varchar(255) NOT NULL,
    `pass` varchar(255) NOT NULL,
    `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=user,2=reporter,3=admin',
    `active` int(1) NOT NULL DEFAULT b'0' COMMENT '1=active,0=inactive',
    `soft_delete` bit(1) NOT NULL DEFAULT b'0' COMMENT '1=deleted,0=active',
    `hash` varchar(32) DEFAULT '0',
    PRIMARY KEY (`id`)
    )
    ENGINE=InnoDB";
    $this->db->query($sql);

    $sql = "ALTER TABLE `users_list`
    ADD UNIQUE `email` (`email`)";
    $this->db->query($sql);    
}

private function _createTableFooter() {
    $sql = "CREATE TABLE `footer_links` (
    `id` tinyint(4) NOT NULL AUTO_INCREMENT,
    `title` varchar(50) NOT NULL,
    `link` varchar(255) NOT NULL,
    `icon` blob NOT NULL,
    `active` smallint(1) NOT NULL DEFAULT '0',
    `type` smallint(1) NOT NULL COMMENT '1=Inner,0=Away',
    PRIMARY KEY (`id`)
    )
    ENGINE=InnoDB";
    $this->db->query($sql);

    $sql = "INSERT INTO `footer_links` (`id`, `title`, `link`, `icon`, `active`, `type`) VALUES
    (1, '২২২১', 'http://fb.me', 'img/images.png',   1,  0),
    (2, 'লিংক এক',  '#',    'img/download.png', 1,  0),
    (3, 'লিংক দুই', '#',    'img/r.jpg',    1,  0),
    (4, 'লিংক তিন', '#',    'img/d.jpg',    1,  0),
    (5, 'লিংক চার', '#',    'img/m.png',    1,  0),
    (6, 'রেডিও',    '#',    'img/a.png',    1,  0),
    (7, 'ই-পেপার',  '#',    '#',    1,  1),
    (8, 'বিজ্ঞাপন', '#',    '#',    1,  1),
    (9, 'সার্কুলেশন',   '#',    '#',    1,  1),
    (10,    'পবিত্র হজ',    '#',    '#',    1,  1),
    (11,    'দূর পরবাস',    '#',    '#',    1,  1),
    (12,    'উত্তর আমেরিকা',    '#',    '#',    1,  1)";
    $this->db->query($sql);
}

private function _createTablePerson() {
    $sql = "CREATE TABLE `persons` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `fname` varchar(10) COLLATE utf8_bin NOT NULL,
    `email` varchar(255) COLLATE utf8_bin NOT NULL,
    `lname` varchar(12) COLLATE utf8_bin NOT NULL,
    `nick` varchar(255) COLLATE utf8_bin NOT NULL,
    `phon` varchar(15) COLLATE utf8_bin DEFAULT NULL,
    `address` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
    `location` varchar(20) COLLATE utf8_bin DEFAULT NULL,
    `locality` varchar(20) COLLATE utf8_bin DEFAULT NULL,
    `profile` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `company` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `dob` date DEFAULT NULL,
    `nid` varchar(20) COLLATE utf8_bin DEFAULT NULL,
    `image` mediumblob,
    `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP  ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
    )
    ENGINE=InnoDB";
    $this->db->query($sql);
    $sql = "ALTER TABLE `persons`
    ADD UNIQUE `nick` (`nick`)";
    $this->db->query($sql);    
}
private function _createTableComment() {

    $sql = "CREATE TABLE `comment` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(10) unsigned NOT NULL,
    `cat_name` varchar(100) CHARACTER SET latin1 NOT NULL,
    `sub_name` varchar(100) CHARACTER SET latin1 NOT NULL,
    `post_id` int(11) NOT NULL,
    `comment_text` text NOT NULL,
    `posted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `user_id` (`user_id`)
    )
    ENGINE=InnoDB";
    $this->db->query($sql);
}

private function loadTableData($newsItemNumber) {
    foreach ($this->session->menues as $cat) {
     $subcat = $this->newsdb->getSubmenues($cat['catID']);
     foreach ($subcat as $sub) {
      $tableName = $cat['catEN'] . "_" . $sub['subcatEN'];
      $catid = $sub['catID'];
      $subid = $sub['id'];
      $sql = "INSERT INTO {$tableName}
      (`title`, `content`, `image`, `user`, `created_at`, `updated_at`, `cat_id`, `subcat_id`, `published`, `viewed`
      )
      VALUES ";
//				print_r($sql);exit;
      for ($i = 0; $i < $newsItemNumber; $i++) {
       if ($i) {
        $sql .= ",";
    }
    $viewed = mt_rand(1, 100000);
    $sql .= "('ডামি টাইটেল ফ্রম ডাটাবেজ {$sub['subcatBN']} : {$i}',
    '{$i}Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam est urna, varius in ipsum ac, elementum consectetur risus. Vivamus eget elit nulla. Etiam placerat porttitor dui sit amet pulvinar. Nulla elementum erat sed ultricies laoreet. Duis id erat sed nulla tempus aliquam. Quisque at odio lectus. Donec blandit urna non nulla ullamcorper faucibus. Morbi ornare facilisis tincidunt. Nullam sit amet tristique leo.

    Aenean fermentum ligula in dui aliquam, non dignissim leo commodo. Ut id tincidunt enim. Curabitur elementum vehicula mi id tincidunt. Vestibulum facilisis libero justo, non fringilla augue imperdiet sed. Ut pulvinar tristique est, at euismod diam scelerisque commodo. Nullam at lacus sit amet eros molestie dictum. Donec venenatis lectus nec sapien aliquam sollicitudin. Etiam ut neque lacinia, aliquam sapien et, placerat metus. Praesent non nunc luctus, iaculis augue quis, congue elit. Vestibulum pulvinar sit amet leo sit amet tincidunt. Nunc tristique aliquet erat sed imperdiet. In bibendum nulla at metus fermentum sodales. Nam dolor ex, fermentum ac varius ac, scelerisque eu ligula. Cras mollis ultrices urna. Nunc lorem justo, tristique quis libero at, ornare tempus massa. Maecenas hendrerit finibus augue, at blandit nunc lacinia ut.
    
    Curabitur pharetra metus sit amet leo cursus, id sagittis urna pharetra. Vestibulum sollicitudin justo nec eros tristique commodo. Nam leo enim, condimentum quis bibendum non, cursus at ante. Integer rutrum nibh urna. Nulla fringilla sagittis nunc. Vivamus diam nunc, porta eget sem fermentum, blandit lacinia nulla. Nam porta arcu a suscipit commodo. Vestibulum eu dictum nisl. In at odio eget purus porta consectetur. Nulla tincidunt molestie porta. Cras venenatis ultrices lacus. Etiam dapibus bibendum eleifend.
    
    Curabitur convallis nibh sem, sed sollicitudin orci vestibulum non. In posuere, arcu vitae tempus facilisis, eros dui efficitur arcu, eu vestibulum elit nisl in ligula. Etiam vel vulputate sapien, pellentesque porta velit. Pellentesque porta fringilla odio, a vulputate lorem tempus nec. Fusce non risus velit. Etiam at dolor scelerisque, gravida sapien vel, congue erat. Praesent consectetur dictum mi egestas porta. Phasellus a pharetra leo, auctor tristique massa. Phasellus ut turpis arcu.
    
    Cras ullamcorper dolor id porta laoreet. Donec et dolor sit amet purus tempus ultricies vel eget nibh. Mauris ac sagittis nisl, eu pulvinar nibh. Ut vitae dolor sed felis ornare accumsan in ac orci. Proin vitae orci id nisi porttitor varius. Proin aliquet ligula elit, a cursus neque bibendum sit amet. Suspendisse consequat dolor mauris, eget egestas est auctor nec. Nam tristique sapien vel purus placerat ornare sit amet sollicitudin urna. Ut quis placerat erat. Fusce tristique ante libero, eu ultricies nibh commodo in. Quisque volutpat enim tempus lacinia posuere. Vivamus sodales nulla eu tempor pellentesque. Mauris scelerisque dolor vel metus condimentum, quis feugiat purus mattis. Quisque ac felis dolor. Pellentesque posuere lacus at congue porta.
        ', NULL, '1', now(), '0000-00-00 00:00:00', '{$catid}', '{$subid}', '1', '{$viewed}')";
}
				// print_r($sql);exit;
$this->db->query($sql);

}
}
}
public function dbrel(){
  $this->createRel();

}

private function createRel(){

 // $sql = "ALTER TABLE `users_list` ADD UNIQUE `email` (`email`)";
 // $this->db->query($sql);

 // $s1l = "ALTER TABLE `persons`
 // ADD FOREIGN KEY (`id`) REFERENCES `users_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE";
 // $this->db->query($sql);   

 // $sql = "ALTER TABLE `comment`
 // ADD FOREIGN KEY (`user_id`) REFERENCES `persons` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE";
 // $this->db->query($sql);   

  $sql = "ALTER TABLE `comment`
  ADD FOREIGN KEY (`cat_name`) REFERENCES `catagories` (`catEN`) ON DELETE NO ACTION ON UPDATE CASCADE";
  $this->db->query($sql);

  $sql = "ALTER TABLE `comment`
  ADD FOREIGN KEY (`sub_name`) REFERENCES `subcatagories` (`subcatEN`) ON DELETE NO ACTION ON UPDATE CASCADE";
  $this->db->query($sql);

 // $sql = "ALTER TABLE `persons`
 // ADD UNIQUE `nick` (`nick`)";
 // $this->db->query($sql);    

  foreach ($this->session->menues as $cat)
  {
    $subcat = $this->newsdb->getSubmenues($cat['catID']);
    foreach ($subcat as $sub) 
    {
     $tableName = $cat['catEN'] . "_" . $sub['subcatEN'];
      // $sql = "ALTER TABLE `{$tableName}`
      // CHANGE `image` `image` mediumblob NOT NULL AFTER `content`";
      // $this->db->query($sql);

     // $sql =  "ALTER TABLE `{$tableName}`
     // ADD FOREIGN KEY (`cat_id`, `subcat_id`) REFERENCES `subcatagories` (`catID`, `id`) ON DELETE RESTRICT ON UPDATE CASCADE";
     // $this->db->query($sql);



 }

}

}


private function createNewsTable() {
  foreach ($this->session->menues as $cat) {
   $subcat = $this->newsdb->getSubmenues($cat['catID']);
   foreach ($subcat as $sub) {
    $tableName = $cat['catEN'] . "_" . $sub['subcatEN'];
				/*$sql = "ALTER TABLE `{$tableName}`
                CHANGE `image` `image` mediumblob NOT NULL AFTER `content`";*/

				/*$sql = "UPDATE `{$tableName}` SET
                `viewed` = '0'";*/

				/*                $sql = "UPDATE `{$tableName}` SET
                `content` = 's'";*/

				/*$sql = "ALTER TABLE `{$tableName}`
					                CHANGE `subcat_id` `subcat_id` smallint(3) NULL AFTER `updated_at`,
				*/

				/*            $this->db->query($sql);
					                $sql = "UPDATE `{$tableName}` SET
					                `cat_id` = '{$catid}' ,`subcat_id` = '{$subid}'";
				*/
				/*$sql = "ALTER TABLE `{$tableName}`
					                CHANGE `subcat_id` `subcat_id` int(10) unsigned NULL AFTER      `updated_at`,
				*/

				/*         $sql = "ALTER TABLE `{$tableName}`
                AD  D FOREIGN KEY (`cat_id`) REFERENCES `catagories` (`catID`) ON     DE  LETE RESTRICT ON UPDATE CASCADE";  */
				//   $sql = "DROP TABLE $tableName";
				// $this->db->query($sql);
                $sql = "CREATE TABLE $tableName(
                id INT(11) NOT NULL AUTO_INCREMENT,
                title TEXT,
                content TEXT,
                image MEDIUMBLOB,
                user INT(11) unsigned NOT NULL,
                created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                cat_id smallint(3) UNSIGNED NOT NULL,
                subcat_id smallint(3) UNSIGNED NOT NULL,
                published TINYINT(1),
                viewed INT(11),
                PRIMARY KEY (`id`)
                )
                ENGINE=InnoDB";
                $this->db->query($sql);
            }
        }

    }

    private function _createTableCompany() {
        $sql = "CREATE TABLE `company` (
        `id` tinyint(4) NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `des` varchar(255) NOT NULL,
        `copyright` varchar(10) NOT NULL,
        `editor` varchar(255) NOT NULL,
        `address` varchar(255) NOT NULL,
        `phon` varchar(20) NOT NULL,
        `fax` varchar(20) NOT NULL,
        `email` varchar(255) NOT NULL,
        `logo` mediumblob ,
        PRIMARY KEY (`id`)
        )
        ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $this->db->query($sql);

        $sql = "INSERT INTO `company` (`id`, `name`, `des`, `copyright`, `editor`, `address`, `phon`, `fax`, `email`) VALUES
        (2, 'খবর প্রতিদিন', 'Khobor protidin is the highest circulated and most read newspaper in Bangladesh. The online portal of khobor protidin is the most visited Bangladeshi and Bengali website in the world. Privacy Policy',   '১৯৯৮-২০১৭',    'শামস্ সুজন',   'উত্তরা, ঢাকা ',    '০১৭৭০০৭৫৫৮৯',  '১২৩৪৫৬৭',  'suspathan@gmail.com'  )";
        $this->db->query($sql);

    }

    private function email() {

        $to = 'user.beshi@gmail.com';
        $subject = 'Signup | Verification';
        $message = 'Hello';
        $headers = 'From: Registration Office';
        if (mail($to, $subject, $message, $headers)) {
         echo "success";
     } else {
         echo "not sent";
     }

 }
}
