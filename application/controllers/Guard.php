<?php
include_once dirname(__FILE__) . "/User.php";

class Guard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('my_helper');
        $this->load->model('Users_Model', "userDB");
        $this->load->model('Persons_Model', "personDB");
    }

    public function index()
    {
        redirect(base_url(), 'refresh');
        die();
    }

    public function register()
    {
        $this->load->view('user/register');
    }

    public function forgot()
    {   

        if(array_key_exists('forgot', $this->input->post()) &&
                                array_key_exists('email', $this->input->post()) &&
                                $email = sanitizeSting($this->input->post('email'))
                                ) {
          $code = rand(100000,1000000);
          $salt = "asharMasherVashaPani";
          $token = md5(md5($salt).$code);
          // save hashed token to database
          $save = $this->userDB->userToken($email, $token);
          $_SESSION['forgot']['email'] = $email;

          if($save) {
            // ******** send code to mail
            $to      = $email;
            $subject = 'নতুন চাবি\'র আবেদন';
            $message = 'আপনার ইমেইল ঠিকানা ('.$email.') থেকে নতুন চাবির জন্য অনুরোধ করা হয়েছে। <br>আপনার নিরাপত্রা যাতাইকরণ কোডটি হলো : <strong>'.$code.'</strong><br> ক‌োডটি আপনি যাচাইকরণ পৃষ্ঠায় প্রদানের মাধ্য‍মে নতুন চাবি ঠিক করতে পারবেন';
            $headers = 'From:'.base_url();
            $mailGo = mail($to, $subject, $message, $headers);
            
            if($mailGo){
                $_SESSION['notice']['success'] = "Enter Your code you got from your email";
            } else {
                $_SESSION['error']['error'] = "Couldn't send email, try again later";
            }

          } else {
            $_SESSION['notice']['error'] = "Couldn't reset password";
          }
            $this->load->view('user/reset');

        } elseif (array_key_exists('reset', $this->input->post()) &&
        array_key_exists('code', $this->input->post()) &&
        $code = sanitizeSting($this->input->post('code'))
        ) { 
            $salt = "asharMasherVashaPani";
            $token = md5(md5($salt).$code);
            $userEmail = $_SESSION['forgot']['email'];
            $_SESSION['forgot']['token'] = $token;
            $matched = $this->userDB->matchToken($userEmail, $token);
            
            if($matched == true){

                $this->load->view("user/resetPass");
                
            } else {

                $_SESSION['forgot']['error'] = "Your varification code is invalid";
                $this->load->view('user/reset');
                
            }

        } else {
          $this->load->view('user/forgotpassword');
        }

    }

   public function newPassword(){
        $required = array('email', 'token', 'password', 'passwordConfirm');
        
         $data = $this->input->post();

        if (count(array_intersect_key(
            array_flip($required), $data)
            ) === count($required)) {
            $data['email'] = sanitizesting($this->input->post('email'));
            $data['token'] = sanitizesting($this->input->post('token'));
            $password = sanitizesting($this->input->post('password'));
            $passwordConfirm = sanitizesting($this->input->post('passwordConfirm'));
            
            if($password == $passwordConfirm){

                $data['password'] = sha1($data['password']);
                $request = $this->userDB->changePassword($data);
                if($request == 'done'){
                    $_SESSION['notice']['success'] = "Password change successfull, login with new password";
                    $this->load->view("user/login");                    
                } else {
                    $_SESSION['notice']['error'] = "Password couldn/t be changed";
                    $this->load->view("user/login");
                }
            } else {
                $_SESSION['notice']['error'] = "Please enter both password same";
                $this->load->view("user/reset");
            }
            
        } else {
            $_SESSION['notice']['error'] = "Please enter correctly, try again";
            $this->load->view("user/reset");
        }
        
   }
    public function login()
    {
        if (isset($_SESSION['user'])
            && $_SESSION['user']['loggedIn'] == true)
        {
            redirect(base_url(), 'refresh');
        } else {
            $this->load->view('user/login');
        }
    }

    public function letMeLeave()
    {
        $_SESSION['user'] = null;
        unset($_SESSION);
        session_destroy();
        redirect(base_url(), 'refresh');
    }

    public function letMeIn()
    {   //sanitizing
        $user['email'] = sanitizeSting(
            $this->input->post('email'));

        $user['pass'] = sha1(sanitizeSting(
            $this->input->post('password')));

        if ($user['email'] && $user['pass']) {
            //get user status from model
            $suspectUser = $this->userDB->isValid($user);
            //if valid then isValid will return userID in "id" key
            // and "registered" in "status" key
            //if registered but not varified then "pending" in status key
            if($suspectUser['status'] == "wrongPass"){
                $_SESSION['error'] = "আপনার প্রবেশ গুপ্তচাবি সঠিক নয়, সঠিক চাবি ব্যবহার করুণ , অথবা <a href=".base_url()."guard/forgot>নতুন চাবির জন্য অনুরোধ করুন। </a>";
                redirect(base_url() . 'guard/login', 'refresh');
                die();
            } elseif ($suspectUser['status'] == "registered") {

                $_SESSION['user']['loggedIn'] = true;
                $_SESSION['user']['id'] = $suspectUser['id'];
                $_SESSION['user']['email'] = $user['email'];


                if (isset($_SESSION['currentPost'])) {
                //if login from any post then back to that post
                    $p = $_SESSION['currentPost'];
                    redirect(base_url() .
                        $p['cat'] .
                        "/" .
                        $p['sub'] .
                        "/article/" .
                        $p['postID'], 'refresh');
                }
                redirect(base_url(), 'refresh');

            } elseif($suspectUser['status'] == "pending")
            {
                $_SESSION['error'] = "আপনার একাউন্টটি যাচাই করা হয়নি। যাচাই নির্দেশনার জন্য আপনি আপনার মেইল চেক করুন";
                redirect(base_url() . 'guard/login', 'refresh');
                die();
            } else
            {

                $_SESSION['error'] = " দু:খিত , আপনি নিবন্ধিত নন।";
                redirect(base_url() . 'guard/login', 'refresh');
                die();
            }
        } else {

            $_SESSION['error'] = "Empty Value. Please fillup both";
            redirect(base_url() . 'guard/login', 'refresh');
            die();

        }
    }

    public function askMore()
    {
        $this->load->view('user/askMore');
    }

    public function varifyUser(){
        /* WHen user click link in Email to varify new account*/
        $email = sanitizeSting($this->input->get('email'));
        $hash = sanitizeSting($this->input->get('hash'));
        if($email && $hash){
            $active = $this->userDB->activeUser($email, $hash);
            if($active)
                echo "Varified!! you can now log in";
            else
                echo "Invalid Varification";
        }
        else {
            die("Something went  wrong");
        }
    }

    private function _saveUser()
    {

        $_SESSION['userRegiInfo']['hash'] = md5( rand(0,1000) );
        $this->user = $_SESSION['userRegiInfo'];
        $status = null;

        $addUser = $this->userDB->addUser($this->user);
        $this->user['id'] = $this->db->insert_id();

        if($addUser == 'done'){
        // ******** email
//        echo $this->user['email'];exit;
            $to      = $this->user['email'];
            $subject = 'রেজীস্ট্রেশান যাচাইকরণ';
            $varifyLink = base_url()."guard/varifyUser?email=".$this->user['email']."&hash=".$this->user['hash'];
            $message = 'হ্যালো '.$this->user['firstName'].'!আপনার একাউন্ট ভেরীফাই করতে এখানে ক্লীক করুন '.$varifyLink;
            $headers = "From:Office";
            $mailGo = mail($to, $subject, $message, $headers);
            if($mailGo){
                $addPerson = $this->personDB->createPerson($this->user);
                $status = 'done';
            } else {
                $status = 'mailNotSent';
            }

        } else {

            $status = $addUser;
        }

        return $status;

        //*******email end

    }

    public function addUser()
    {
        $user['firstName'] = sanitizeSting(
            $this->input->post('firstName'));

        $user['lastName'] = sanitizeSting(
            $this->input->post('lastName'));

        $user['email'] = sanitizeSting(
            $this->input->post('email'));

        $pass1 = sanitizeSting(
            $this->input->post('passwordOne'));

        $pass2 = sanitizeSting(
            $this->input->post('passwordTwo'));
        /**
        Password check
         **/
        if ($pass1 == $pass2) {

            $user['pass'] = sha1($pass2);

        } else {

            $this->error['pass'] = "আপনার দেয়া গুপ্তসংকেত দুটি একই নয়";

        }

        if (
            empty($user['firstName']) ||
            empty($user['lastName']) ||
            empty($user['email'])
        ) {
            $this->error['empty'] = "আপনি সবগুলো খালিঘর পূরণ করেননি";
        }

        /**
        If OK
         **/
        if (!isset($this->error)) {

            $_SESSION['userRegiInfo'] = $user;
            if ($status = $this->_saveUser()) {

                if($status == 'done'){
                    $_SESSION['success'] = true;

                } elseif($status == 'undone'){
                        $this->error['undone'] = "দু:খিত, আপনার তথ্য নিবন্ধনে কোথাও কোনো সমস্যা হয়েছে";
                    } elseif($status == 'duplicate'){
                        $this->error['duplicate'] = "আপনি আগে থেকেই নিবন্ধিত, প্রবেশ করুন";
                    }
            } else {
                $this->error['UserNotSaved'] = "কোথাও কোনো ভুল হয়েছে আপনার তথ্য সংরক্ষন করা যায়নি! দু:খিত ।";
            }
        }  
        $_SESSION['error'] = $this->error;
        redirect(base_url() . 'guard/register', 'refresh');
        die();

    }
}
