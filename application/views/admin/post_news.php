<?php
//    print_r($this->session->submenues);exit;

$postPATH = base_url()."dashboard/post_news";
include_once("header.php");
include("sidebar.php");

if(isset($message['success']) && $message['success']!=null){
?>
<div id="alert-danger" class="row col-md-10 alert alert-dismissible alert-success">
  <?php echo $message['success'] . "<br>";
  ?>
</div>
<?php
}
if(isset($message['error']) && $message['error']!=null){
?>
<div id="alert-danger" class="row col-md-10 alert alert-dismissible alert-danger">
  <?php echo $message['error'] . "<br>";
  ?>
</div>
<?php
}
?>



<div class="row col-md-10">
<form action="<?=$postPATH?>" method="post" enctype="multipart/form-data">
         <div class="form-group col-md-6" id="catagory" >
            <label class="control-label col-md-7">Select Catagory </label>
                <div class="col-md-5">
                    <select name="catid" class="form-control input-md" id="catname">
                        <option>none</option>
                        <?php
                    foreach($this->session->menues as $key=>$cat){
                        echo "<option value=";
                        echo $cat['catID'].">";
                        echo $cat['catBN'];
                        echo "</option>";
                    }
                        ?>
                    </select>
                </div>
        </div>

        <div class="form-group col-md-6" id="subcatagory">
            <label class="control-label col-md-7">Select Subcatagory </label>
                <div class="col-md-5">
                    
                <select name="subid" class="form-control input-md" id="subname">
                        <option>none</option>
                        
                    </select>
                </div>
        </div>
        
        <div class="form-group col-md-6" id="title">
            <label class="control-label col-md-7">Title </label>
                <div class="col-md-7">
                    <input type="text" name="title" style="width:180%">
                </div>
        </div>
        <div class="form-group col-md-6" id="photo">
            <label class="control-label col-md-7">Upload Photo </label>
                <div class="col-md-7">
                    <input type="file" name="image">
                </div>
        </div>
        <div class="form-group col-md-12" id="content">
            <div class="content-box-large">
               <div class="panel-heading">
                    <div class="panel-title">Content</div>
                </div>
                <div class="panel-body">
                    <textarea id="bootstrap-editor" name="content" placeholder="Enter text ..." style="width:98%;height:200px;"></textarea>
                </div>
            </div>
         </div>
         <div class="action col-md-12" id="submit">
          <input type="submit" name="post" class="btn btn-success" value="post">
          <button class="btn btn-primary btn-lg " value="preview">Preview</button>
		</div>
</form>
</div>


<?php

include("footer.php");
?>