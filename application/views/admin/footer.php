
    </div>


<footer>
         <div class="container">
         
            <div class="copy text-center">
               Copyright 2018 <a href='#'>Shams</a>
            </div>
            
         </div>
      </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=base_url()?>js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=base_url()?>admin/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>admin/js/custom.js"></script>

<script>

function getsub(id){
  $("#subname").find("option").remove();
  $("#subcatagory").fadeIn();
      $.ajax({
        url: "<?=base_url()?>ajax/getsub/" + id,
        method: "GET"
      }).
      done(function(data){
          var foo = JSON.parse(data);
            for(i in foo){
            $("#subname").append("<option value='"+foo[i].id+"'>"+foo[i].subcatBN+"</option>");
          }
        }).
      fail(function(){        
        $("#subname").append("<option>No data loaded</option>").css("background-color","red");
      });
}

function showForm(){
  $("#title").fadeIn("slow");
  $("#photo").fadeIn("slow");  
  $("#content").fadeIn("slow");
  $("#submit").fadeIn("slow");  
}

$("#subcatagory").hide();
$("#title").hide();
$("#content").hide();
$("#photo").hide();
$("#submit").hide();

$("#catname").click( ()=>{
            getsub(encodeURIComponent($("#catname").val()));
          });
$("#subname").click( ()=>{
             showForm();
          });


</script>




</body>
</html>