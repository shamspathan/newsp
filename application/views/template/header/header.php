<?php
$menues = null;
if (isset($this->session->menues)) {
    $menues = $this->session->menues;
}
if (isset($this->session->company)) {
    $company = $this->session->company;
}


$userEmail = null;

if (isset($_SESSION['user']) && isset($_SESSION['user']['email'])) {
    $userEmail = $_SESSION['user']['email'];
    //print_r($_SESSION['user']);exit;
}
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
    <!-- Basic Page Needs
    	================================================== -->
    	<meta charset="utf-8">
    	<title><?=$company['name']?></title>
    	<meta name="description" content="A news provider">
    	<meta name="author" content="www.facebook.com/supathan">

    <!-- Mobile Specific Metas
    	================================================== -->
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    	================================================== -->
    	<link rel="stylesheet" href="<?=base_url()?>css/bootstrap.css">
    	<link rel="stylesheet" href="<?=base_url()?>css/zerogrid.css">
    	<link rel="stylesheet" href="<?=base_url()?>css/style.css">
        <link rel="shortcut icon" href="data:image/x-icon;" type="image/x-icon">

        <script src="<?=base_url()?>js/jquery-3.3.1.min.js"></script> 
        <style type="text/css">
        
        #logo>a {
            position: "relative";
            float: "left";
            margin-top: 0px;
            height: 60px ;
            width: 60% 
        }
        #topRight > a > button {
            border-top-left-radius:50%;
            border-bottom-right-radius:50%;
        }  

        #commentNotice{
        	margin-top: 2%;
	        margin-bottom: 30%;	
        }
        .top-menu {
            background-color:rgb(90, 90, 90);
            width: 100%;
            margin:0px;
            border-bottom-right-radius:60%;
        }
        .containerAd{
            width:100%;
            height: 150px;
            border-radius:20%;
        }

    </style>

</head>
<body style="position:'relative'; margin:0px; background-color: none">

    <div class="wrap-body">
        <div class="top-menu fixed-top"> <!--Upper fixed part -->
          <div id="logo">
            <a href="<?=base_url()?>">
                <?php if($this->session->company['logo']): ?>
                    <img src="data:image/jpeg;base64,<?=base64_encode($this->session->company['logo'])?>"/>
                <?php else: ?>
                    <img style="height:60px ;width:40%" src="<?=base_url()?>img/header.jpg"/>
                <?php endif ?>

            </a>
            <div id="topRight" style="float:right;margin-top:10px" >

                <?php
                if ($userEmail) {
                    ?>
                    <span class="badge badge-primary btn-sm"><?=$userEmail?></span>
                    <a href="<?=base_url()?>user/profile">
                        <button type="button" class="btn btn-secondary btn-sm">তথ্যসংক্ষেপ</button>
                    </a>
                    <a href="<?=base_url()?>guard/letMeLeave" >
                        <button type="button" class="btn btn-secondary btn-sm">বাহির</button>
                    </a>

                    <?php
                } else {
                    ?>
                    <a href="<?=base_url()?>guard/login" >
                        <button type="button" class="btn btn-primary btn-sm">প্রবেশ</button>
                    </a>



                    <a href="<?=base_url()?>guard/register" >
                        <button type="button" class="btn btn-secondary btn-sm">নিবন্ধন</button>
                    </a>
                    <?php
                }
                ?>
            </div>

        </div>
        <div style="clear: both"></div>        
        <!--//////////////////////////////////////Menu-->
        <div id = "menu" class=" " >
            <nav class=" navbar navbar-expand-lg navbar-dark bg-primary" style="padding:4px;border-top-left-radius:20%;border-bottom-right-radius:20%">
                <ul class="navbar" style="padding:2px;">
                    <?php
                    $count = 1;
                    foreach ($menues as $menu) {
                        echo "<li class='nav-item'><a href='". base_url() . $menu['catEN'] . "'><button style='border-top-left-radius:60%;border-bottom-right-radius:60%' type='button' class='btn btn-primary btn-sm'>" . $menu['catBN'] . "</button> </a></li>";
                        if ($count++ > 7) {
                            break;
                        }
                    }
                    ?>
                </ul>
            </nav>
        </div>
    </div> <!--Upper fixed part -->
    <div style="clear: both;position:relative ;padding-top: 100px"></div>

<!--     <script type="text/javascript">
        $(window).bind('scroll', function () {
            if ($(window).scrollTop() > 50) {
                $('.top-menu').addClass('fixed-top').fadeOut("slow");
            } else {
                $('.top-menu').removeClass('fixed-top');
                $('.top-menu').addClass('fixed-top').fadeIn("slow");
            }
        });    

    </script>
 -->