<?php
//print_r($newsOne);exit;

?>
<?php include "submenulist.php"?>

<section id="container" class="sub-page">
<div class="wrap-container zerogrid">

<?php include 'subHistoryButton.php';?>

<div id="main-content" class="col-2-3">
	<div class="wrap-content">
		<article>
			<div class="art-header">
				<div class="entry-title">
					<h2><?=$newsOne['title']?></h2>
				</div>
				<div class="info">By <?=$newsOne['reporter']?> on <?=$newsOne['created_at']?></div>
			</div>
			<div class="art-content">
<?php if($newsOne['image']): ?>
				<img style="min-height:300px ; min-width: 700px;" src="data:image/jpeg;base64,<?=base64_encode($newsOne['image'])?>"/>
<?php else:  ?>
				<img style="min-height:50% ; min-width:60%" src="<?=base_url()?>img/demo.jpg"/>
<?php endif?>


				<div><?=$newsOne['content']?>
				</div>
			</div>
		</article>



		<!-- ######## COmment Start ##################################### -->
		<div class="widget wid-related">
			<div class="wrap-col">

				<!--######### user comment post ######### -->
				<?php include_once 'comments/showComments.php';?>
				<!-- ######### user comment show block end ######### -->


			</div>
		</div>
		<!-- ############ comment post end ################ -->
<!-- #############################################
				#######################				Comment section
			-->

			<?php include 'comments/postComment.php';?>

			<!-- ######################Comment post section END-->


		</div>
	</div>

</div>
</section>