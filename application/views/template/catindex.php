<?php
//print_r($catInfo);
?>

<?php include 'submenulist.php'; ?>


<section id="container" class="sub-page">
	<div class="wrap-container zerogrid">
		
		<!-- ######## #################### -->
		<?php include 'subHistoryButton.php'; ?>
		<!-- ######## #################### -->

		<?php
		$addCount = 1; //for showing add in order
		foreach($submenues as $menu){ ?>
		
		<div class="zerogrid" style="margin-top:10px">
			<div class="row wrap-box"><!--Start Box-->
				<div style="text-align: right;">
					<a href="<?=base_url().$catInfo['catEN']."/".$menu['subcatEN']?>"><span style="padding-top:5px" class='badge badge-default'><h1><?=$menu['subcatBN']?></h1></span></a>
				</div>
				
				
				<div class="row">
					<?php
					$count = 0 ;
					foreach($news[$menu['subcatBN']] as $item){
						if(++$count>3)break;
						include 'newsblock.php';
					} ?>
				</div>

			</div>
		</div>
					<!-- Showing add -->
					<?php if(($addCount%2) == 0){
				echo '<div class="container containerAd"><img class="containerAd" src="'.base_url().'img/ad/ad'.$addCount.'.jpg"></div>';
			}
			$addCount++;
	}
	?>



</div>
</section>