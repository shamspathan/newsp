<style>

.container {
  margin-bottom: 370px;
	margin-left : -280px;
}

.form-control {
	border:none;
}
.form-control > input {
	border :1px solid yellow;
	border-radius: 15%;
	padding-top: 6px;
  padding-left: 20px;
	padding-bottom: 5px;
	width:0px;
}
#alert-danger {
  position: relative;
  width : 100%;
  margin: 10px 5px;
}
.alert {
  text-align: center;
}
</style>


<body>

<?php
if (isset($_SESSION['notice']['success'])) {
  ?>
<div id="alert-success" class="alert alert-dismissible alert-success">
  <?php echo $_SESSION['notice']['success'] . "<br>";
  ?>
</div>

<?php
$_SESSION['notice']['success'] = null;

}
?>

<?php
if (isset($_SESSION['notice']['error'])) {
  ?>
<div id="alert-danger" class="alert alert-dismissible alert-danger">
  <?php echo $_SESSION['notice']['error'] . "<br>";
  ?>
</div>
<?php
$_SESSION['notice']['error'] = null;

}
?>

	<div class="container" >

  <form action="<?=base_url()?>guard/forgot" method="POST"" method="POST">
    <div class="form-group row">
      <label for="code" id="code" class="col-sm-2 col-form-label"> সংকেত </label>
      <div class="col-sm-10">
        <div class="form-control">
          <input type="text" class="form-control" id="code" name="code" >
        </div>
      </div>
    </div>


    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
          <div class="form-control">
          <input type="hidden" id="email" name="email"></input>
            <input type="submit" id="reset" name="reset" class="btn btn-primary" value=" নিশ্চিত "></input>
          </div>
      </div>
    </div>
  </form>
</div>

<script type="text/javascript">
$(".container").animate({marginTop:"40px",marginLeft : "15%"} , 600);
$(".form-control>input").animate({width:"60%"} , 700);
$("#alert-danger").fadeIn("slow");

</script>
