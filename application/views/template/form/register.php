<?php
/**
 ** will go to ***** guard/addUser ******
 **/

if (isset($_SESSION['error']) && $_SESSION['error']!='') {
  echo "<div class=\"alert alert-dismissible alert-danger\">";
  echo "<strong>আপনার তথ্য প্রবেশে নিচের ভুলগুলো হয়েছে!</strong>";
  foreach ($_SESSION['error'] as $key => $value) {
    ?>
      <?php echo "<p>" . $value . "</p>";
      ?>
    <?php
    $_SESSION['error'] = null;
  }
  echo "</div>";
}

if (isset($_SESSION['success'])) {
  ?>
  <div class="alert alert-dismissible alert-success">
    <strong>আপনার তথ্য নথিভূক্ত হয়েছে!</strong>
    <?php echo "পরবর্তী নির্দেশনা আপনার দেয়া ইমেল ঠিকানায় পাঠানো হয়েছে <br>";
    ?>
  </div>
  <?php
  $_SESSION['success'] = null;
}

?>
<style>

.container {
  margin-left : -280px;
  margin-bottom: 300px;
}
.alert {
  text-align: center;
}
.form-control {
  border:none;
}
.form-control > input {
  border :1px solid yellow;
  text-align: left;
  border-radius: 15%;
  padding-top: 6px;
  padding-left: 20px;
  padding-bottom: 5px;
  width:0px;
}
#submit {
  border :none;
  text-align: center;
}

#alert-danger {
  display :none;
  position: relative;
  width : 100%;
  margin: 10px 5px;
}

</style>

<div class="container" >

  <form action="<?=base_url()?>guard/addUser" method="POST"" method="POST">
    <div class="form-group row">
      <label for="userName" id="nameLabel" class="col-sm-2 col-form-label"> নাম </label>
      
      <div id="userName" class="col-sm-10 row">
        <div class="form-control col-sm-5">
          <input type="text" class="form-control" id="userFirstName" name="firstName" placeholder="নাম" >
        </div>
        <div class="form-control col-sm-5">
          <input type="text" class="form-control" id="userSecondName" name="lastName" placeholder="পদবী">
        </div>
      </div>

    </div>

    <div class="form-group row">
      <label for="email" id="emailLabel" class="col-sm-2 col-form-label"> ইমেইল ঠিকানা </label>
      <div class="col-sm-10">
        <div class="form-control">
          <input type="email" class="form-control" id="email" name="email" >
        </div>
      </div>
    </div>

    <div class="form-group row">
      <label for="password" class="col-sm-2 col-form-label">গুপ্তসংকেত</label>
      <div class="col-sm-10">
        <div class="form-control">
        <input type="password" class="form-control" id="passwordOne" name="passwordOne">
      </div>
      </div>
    </div>

    <div class="form-group row">
      <label for="password" class="col-sm-2 col-form-label">গুপ্তসংকেত পুন:রায়</label>
      <div class="col-sm-10">
        <div class="form-control">
        <input type="password" class="form-control" id="passwordTwo" name="passwordTwo">
      </div>
      </div>
    </div>

    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
          <div class="form-control">
            <input type="submit" id="submit" class="btn btn-primary" value="উক্ত তথ্য নিবন্ধিত হউক"> </input>
          </div>
      </div>
    </div>
  </form>
</div>

<script type="text/javascript">
$(".container").animate({marginTop:"40px",marginLeft : "15%"} , 600);
$(".form-control>input").animate({width:"60%"} , 700);
$("#alert-danger").fadeIn("slow");

</script>



</div>