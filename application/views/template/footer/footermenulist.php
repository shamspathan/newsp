<?php
$menues = null;
if (isset($_SESSION['menues'])) {
    $menues = $_SESSION['menues'];
}
?>
<div class="wrap-container zerogrid" style="margin-top:40px">

	<?php foreach ($menues as $menu) {?>
	<a href='<?=base_url() . $menu['catEN']?>'>
		<button style="margin: 4px" class="btn btn-secondary btn-sm"><?=$menu['catBN']?></button>
	</a>

	<?php }?>

</div>