<!--////////////////////////////////////Footer-->
<?php include 'footermenulist.php';
$company = null;
$footer = null;

if (isset($this->session->company)) {
    $company = $this->session->company;
} else {
    die("Please active cookie to let website function properly(company)");
}

if (isset($this->session->footer)) {
    $footer = $this->session->footer;
} else {
    die("Please active cookie to let website function properly(footer");
}

?>

<footer class="zerogrid">
	<div class="wrap-footer">
		<div class="row">
			<div class="col-1-3">
				<div class="wrap-col">
					<h4></h4>
					<div class="row">
						<p>
							© স্বত্ব <?=$company['name']?> <?=$company['copyright']?><br>
							সম্পাদক ও প্রকাশক: <?=$company['editor']?><br>
							<?=$company['address']?><br>
							ফোন: <?=$company['phon']?>, ফ্যাক্স: <?=$company['fax']?>
							<br>ইমেইল: <?=$company['email']?>
						</p>
					</div>
				</div>
			</div>
			<div class="col-1-3">
				<div class="wrap-col">
					<?php
foreach ($footer['linksAway'] as $item) {

    echo "<li><a href='" . $item['link'] . "'>" . $item['title'] . "</a></li>";
}

?>
				</div>
			</div>
			<div class="col-1-3">
				<div class="wrap-col">
					<?=$company['des']?>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright">
		<div class="wrapper">
			Copyright 2018 - Designed by <a href="https://www.facebook.com/user.beshi"> শামস্ সুজন </a>
			<ul class="quick-link f-right">
				
			</ul>
		</div>
	</div>
</footer>

<!---End Container div -->
</div>

</body>
</html>