<?php

class Users_model extends CI_Model
{

    public $userTable;

    public function __construct()
    {

        parent::__construct();
        $this->tbl = 'users_list';
    }
    

    ///////////////////  Change Password
    
    public function changePassword($data){

        $signal = null;
        $sql = "SELECT * FROM `users_list` WHERE `email`='".$data['email']."' AND `hash`='".$data['token']."' ";         
        $query = $this->db->query($sql);
        
        if($query->num_rows() > 0){
            
            $sql = "UPDATE `users_list` SET `hash`= 0 , `pass`='".$data['password']."' WHERE `email`='".$data['email']."' AND `hash`='".$data['token']."' ";
            $query = $this->db->query($sql);
            $signal = 'done';
            
        } else {
            $signal = 'sorry';
        }


        return $signal;
    
    }


    public function matchToken($email, $token) {

    $sql = "SELECT `id` FROM `users_list` WHERE `email`='${email}' AND `hash`= '${token}' LIMIT 1";
  
    $query = $this->db->query($sql);
    $status = null ;
    if($query->num_rows()){
        $id =  $query->result_array()[0]['id'];
        $status = $id;   
     } else {
        $status = false;
     }
     return $status;
    }

    public function userToken($email, $token){
        $sql = "UPDATE `users_list` SET `hash`= '${token}' WHERE `email` = '${email}'";
        $query = $this->db->query($sql);
        if($query){
        return true;
        } else {
        return false;
        }

    }
    public function resetPassword($token){
        $email = $_SESSION['forgot']['email'];
        if($email && $token){
            $sql = "SELECT * FROM `users_list` WHERE `hash` = '${token}' AND `email` = '${email}'";
            $query = $this->db->query($sql);
            if($query->num_rows() > 0){
                $status = true;
            } else {
                $status = false;
                }
        } else {
                $status = false;
        }
    }

    public function activeUser($email, $hash){
        $sql = "SELECT * FROM users_list WHERE hash='{$hash}' AND email='{$email}'";
        $do = $this->db->query($sql);
        if($do->result_array()){
            $sql = "UPDATE users_list SET active = '1', hash='0' WHERE hash='$hash' AND email='$email' ";
            $do = $this->db->query($sql);
        } else $do = null;

        if($do)
            return true;
        else
            return false;
    }
    public function addUser($user)
    {
        $status = null ;

        $sql = "SELECT `email` FROM `{$this->tbl}` WHERE `email`='{$user['email']}' LIMIT 1";
        $query = $this->db->query($sql);
     
        if($query->num_rows() == 0){ // no user found and ready to enter as new user
        //  print_r($user);exit;
            $sql = "INSERT INTO " . $this->tbl . " ( email, pass, hash ) " .
            "VALUES('" .
            $user['email'] . "','" .
            $user['pass'] . "','" .
            $user['hash'] . "')";
            $result = $this->db->query($sql);
            if ($result) {
                $status = 'done';
            } else {
                $status = 'undone';
            }            
        } else {
            $status = 'duplicate';
        }

        return $status;


    }

    public function isValid(array $user)
    {
        /**
        Take  User Info Array and return id if exists
         **/

        $sql = "SELECT `id`,`active`,`hash`,`pass` FROM users_list WHERE email='{$user['email']}' LIMIT 1";

        $query = $this->db->query($sql); 

        if ($query->result_id->num_rows) {

            $status = $query->result_array()[0];

            if($status['pass'] != $user['pass']){
                $user['status'] = "wrongPass";
            } elseif($status['active'] == 1)
            {
                $user['id'] =  $status['id'] ;
                $user['status'] = "registered";
            } elseif($status['hash'] != null)
            {
                $user['status'] = "pending";
            } 
        } else
        {   
            $user['status'] = '';
        }

        return $user;

    }

    public function fetch_data_two_con($firstColVal, $secondColVal, $firstColName, $secondColName, $tblName)
    {

        $this->db->where($firstColName, $firstColVal);
        $this->db->where($secondColName, $secondColVal);
        $query = $this->db->get($tblName);
        return $query->result();
    }

    public function insert_user($userData)
    {
        return $this->insert_data($this->userTable, $userData);
    }

    public function insert_data($table, $data)
    {
        if ($this->db->insert($table, $data)) {
            return true;
        } else {
            return false;
        }

    }

    public function get_user_list($userType = 'Readers')
    {
        //echo $userType;exit;
        switch ($userType) {
            case "Admins":return $this->get_data_where('type', '3');
            case "Reporters":return $this->get_data_where('type', '2');
            case "Readers":return $this->get_data_where('type', '1');
            default:return $this->get_data_all();
        }
    }
    public function getUser(int $id)
    {
        $query = $this->db->where("id = {$id}")->get($this->userTable);
        return $query->result_array()[0];
    }
    public function get_data_all()
    {
        $query = $this->db->get($this->userTable);
        return $query->result_array();
    }

    public function get_data_where($field, $val)
    {
        $this->db->where($field, $val);
        $query = $this->db->get($this->userTable);
        return $query->result_array();
    }
    public function query($query)
    {

        if ($this->db->query($query)) {
            return "success";
        } else {
            return "failed";
        }

    }

}
