<?php

class AboutNews extends CI_Model {

	/**
	 * @var mixed
	 */
	public $tblName;
	/**
	 * @var mixed
	 */
	public $tablName2;
	/**
	 * @var mixed
	 */
	public $limit;

	public function __construct() {
		parent::__construct();
		$this->tblName2 = 'subcatagories';
	}

	/**
	 * @param $tag
	 * @return mixed
	 */
	public function post_news($post = null){
		$status = null;
		if($post!=null){
			// title,content,user,cat_id,subcat_id 
			$tableName = $post['catName']."_".$post['subName'];
			$this->db->insert($tableName, $post['data']);

		}
		return $status;
	}
	public function loadComment($dataArray) {

		$sql = "SELECT * FROM comment" .
			" WHERE post_id = ? AND cat_name = ? AND sub_name = ?";
		$query = $this->db->query($sql,
			array(
				$dataArray['postID'],
				$dataArray['cat'],
				$dataArray['sub'],
			)
		);

		if ($query->num_rows()) {
			return $query->result_array();
		} else {
			return null;
		}

	}
	public function getMenues() {

		$this->tblName = 'catagories';
		return $this->getDataAllMenues();
	}
	public function getUserInfo($userID) {
		$sql = "SELECT * FROM persons WHERE id = {$userID}";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			return $query->result_array()[0];
		} else {
			return null;
		}

	}

	public function getReporter($id) {
		$sql = "SELECT nick FROM persons WHERE id='{$id}' ";
		$query = $this->db->query($sql);
		$result = $query->result_array()[0]['nick'];
		return $result;
	}

	public function getFooter($tag) {
		switch ($tag) {
		case 'linksAway':
			$sql = "SELECT * FROM footer_links WHERE active='1' AND type='0'";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			break;
		case 'linksInner':
			$sql = "SELECT * FROM footer_links WHERE active='1' AND type='1'";
			$query = $this->db->query($sql);
			return $query->result_array();

		default:
			return "No tag given";
		}
		return $result;
	}

	/**
	 * @return mixed
	 */
	public function getCompanyInfo() {

		$tableName = "company";
		$sql = "SELECT * FROM company where 1";
		$query = $this->db->query($sql);
		$result = $query->result_array()[0];
		return $result;
	}

	/**
	 * @param $menues
	 * @return mixed
	 */
	public function get6ForEachCat($menues) {
		/* menu structure
			        0: {
			        catID: "1",
			        catEN: "bangladesh",
			        catBN: "বাংলাদেশ"
			        }
		*/
		$tableName = 'subcatagories';
		$news = [];
		//find submenues for each cat
		foreach ($menues as $menu) {
			$sql = "SELECT * FROM $tableName WHERE catID = '" . $menu['catID'] . "'  LIMIT 6 ";
			$query = $this->db->query($sql);
			/* subcat structure
				            0: {
				            id: "1",
				            subcatEN: "divisions",
				            subcatBN: "বিভাগ",
				            catID: "1"
				            }
			*/
			//get news for each submenues
			$submenus = $query->result_array();
			foreach ($submenus as $submenu) {
				$newsTable = $menu['catEN'] . "_" . $submenu['subcatEN'];

				$sql = " SELECT *
                FROM
                ( SELECT * FROM $newsTable WHERE published ORDER BY created_at DESC LIMIT 5
                ) AS tmp
                ORDER BY viewed DESC LIMIT 1";

				$result = $this->db->query($sql);
				$news[$menu['catEN']][] = $result->result_array()[0];
			}

		}
		if ($news) {
			foreach ($news as $key => &$items) {
				$items = $this->shortnews($items);
			}
			return $news;
		} else {
			return null;
		}

	}

	/**
	 * @param $catName
	 * @return mixed
	 */
	public function get6ForCat($catName) {

		$tableName = 'catagories';
		$news = [];
		$sql = "SELECT catID FROM $tableName WHERE catEN = '" . $catName . "'";
		$query = $this->db->query($sql);
		$catID = $query->result_array()[0]['catID'];
		$tableName = 'subcatagories';
		$sql = "SELECT * FROM $tableName WHERE catID = '" . $catID . "'";
		$query = $this->db->query($sql);
		$subMenues = $query->result_array();

		foreach ($subMenues as $submenu) {
			$newsTable = $catName . "_" . $submenu['subcatEN'];
			$sql = " SELECT *
            FROM
            ( SELECT * FROM $newsTable WHERE published ORDER BY created_at DESC LIMIT 20
            ) AS tmp
            ORDER BY viewed DESC LIMIT 3";
			$result = $this->db->query($sql);
			$news[$submenu['subcatBN']] = $result->result_array();
		}
		foreach ($news as &$item) {
			$item = $this->shortnews($item);
		}
		return $news;
	}

	/**
	 * @return mixed
	 */
	public function getCats() {

		$this->tblName = 'catagories';
		return $this->getDataAllMenues();
	}

	public function getSubCats() {
		$this->tblName = 'subcatagories';
		return $this->getDataAllSubCats();
	}

	public function saveComment($data) {
		$comment = $data['comment'];
		$sql =
			"INSERT INTO comment (" .
			"user_id, cat_name, sub_name, post_id, comment_text) " .
			"VALUES(?,?,?,?,?)";
		$query = $this->db->query($sql,
			array(
				$comment['userID'],
				$comment['post']['cat'],
				$comment['post']['sub'],
				$comment['post']['postID'],
				$comment['comment_text'],
			)
		);

	}
	/**
	 * @param $catID
	 * @return mixed
	 */
	public function getSubmenues($catID) {

		$this->tblName = 'subcatagories';
		return $this->getDataAllSubmenues($catID);
	}

	/**
	 * @return mixed
	 */
	public function getAllSubmenues() {

		$submenues = $this->getDataAllSubmenues($this->tblName2);
		return $submenues;
	}

	/**
	 * @param $subcat
	 * @param $cat
	 * @return mixed
	 */
	public function getContent($subcat, $cat) {

		$this->tblName = $cat . "_" . $subcat;
		return $this->getNewsDataAll();
	}

	/**
	 * @param $cat
	 * @param $subcat
	 * @return mixed
	 */
	public function get12ItemForSub($cat, $subcat, $offset = 1) {
		$this->tblName = $cat . "_" . $subcat;

		if ($offset <= 1) {
			$offset = 0;
		}

		$limit = 12;
		$news = $this->getNewsLimited($offset, $limit);
		if ($news) {
			$news = $this->shortnews($news);
//        print_r($news);exit;
			return $news;
		} else {
			return null;
		}

	}

	/**
	 * @param $news
	 * @return mixed
	 */
	public function shortnews($news) {

		foreach ($news as &$item) {
			$string = $item['content'];
			$item['content'] = mb_substr($string, 0, 200) . '...';
		}
		return $news;
	}

	/**
	 * @param $limit
	 * @return mixed
	 */
	public function getNewsLimited($offset, $limit) {
		// echo "offset=" . $offset . "<br>" . "limit=" . $limit;exit;
		$sql = "SELECT * FROM {$this->tblName} WHERE published ORDER BY created_at DESC" .
			" LIMIT {$offset}, {$limit}";
		$query = $this->db->query($sql);
		if ($query->num_rows() < 12) {
			return null;
		}
		return $query->result_array();
	}

	/**
	 * @param $cat
	 * @param $subcatLimit
	 * @return mixed
	 */
	public function getContentHome($cat, $subcatLimit = 3) {

		$show = $subcatLimit;
		foreach ($this->submenues[$cat] as $submenu) {
			if (!$show) {
				break;
			}

			$this->tblName = $cat . "_" . $submenu['subcatEN'];
			$data[$submenu['subcatEN']] = $this->getNewsLimitOne();
			$show--;
		}
		return $data;

	}

	/**
	 * @param $cat
	 * @return mixed
	 */
	public function getContentFront($cat) {
		foreach ($this->submenues[$cat] as $submenu) {
			$this->tblName = $cat . "_" . $submenu['subcatEN'];
			$data[$submenu['subcatEN']] = $this->getDataByLimit();
		}
		return $data;
	}

	/**
	 * @param $cat
	 * @param $sub
	 * @param $newsID
	 * @return mixed
	 */
	public function getNewsOne($cat, $sub, $newsID) {
		$this->tblName = $cat . "_" . $sub;
		$news = $this->getDataOneCol('id', $newsID);
		if ($news) {
			$viewed = $this->viewed($news['viewed'], 'id', $newsID);
			if ($viewed) {
				return $news;
			} else {
				echo "something went wrong to make it viwed";
				exit;
			}

		} else {
			return false;
		}
	}

	public function viewed($value, $id, $col) {
		$views = (int) $value + 1;
		$sql = "UPDATE {$this->tblName} SET viewed='{$views}' WHERE $id='{$col}'";
		if ($this->db->query($sql)) {
			return true;
		} else {
			return false;
		}

	}
	/**
	 * @param $id
	 * @return mixed
	 */
	public function getCatname($id) {
		$this->tbl = "catagories";
		return $this->getField('catID', $id, 'catEN');
	}

	/**
	 * @param $catid
	 * @param $subid
	 * @return mixed
	 */
	public function getSubcatname($catid, $subid) {
		$this->tbl = "subcatagories";
		$subcatname = $this->getSub($catid, $subid, 'subcatEN');
		return $subcatname;
	}

	/**
	 * @param $catID
	 * @param $subcatName
	 */
	public function checkSub($catID, $subcatName) {
		$tableName = "subcatagories";
		$sql = "SELECT * FROM $tableName where catID = '" . $catID . "' AND subcatEN='" . $subcatName . "'";
		$query = $this->db->query($sql);
//        print_r($catID);exit;
		if ($query->num_rows()) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * @param $catName
	 * @return mixed
	 */
	public function checkCat($catName) {
		$tableName = "catagories";
		$sql = "SELECT catID FROM $tableName where catEN = '" . $catName . "'";
		$query = $this->db->query($sql);
		if ($query->num + rows()) {
			return $query->result_array()[0]['catID'];
		} else {
			return false;
		}

	}

	/**
	 * @param $data
	 */
	public function insertNews($data) {
		$catid = $data['cat'];
		$this->tbl = "catagories";
		$catname = $this->getField('catID', $catid, 'catEN');

		$subcatid = $data['subcat'];
		$this->tbl = "subcatagories";
		$subcatname = $this->getField('id', $subcatid, 'subcatEN');

		$title = $data['newstitle'];
		$content = $data['newscontent'];
		$this->tbl = $catname . "_" . $subcatname;
		$this->insertNewsdata($title, $content, $catid, $subcatid);
	}

	public function getComment($id) {
		$sql = "SELECT * FROM comment WHERE id={$id}";
		$query = $this->db->query($sql);
		$result = $query->result_array()[0];
		return $result;
	}

	public function updateComment($commentID, $comment) {
		$sql = "UPDATE comment SET comment_text='{$comment}' WHERE id='{$commentID}'";
		$query = $this->db->query($sql);
		if ($query) {
			return true;
		} else {
			return null;
		}

	}

	public function deleteComment($commentID, $userID) {
		$sql = "DELETE FROM comment WHERE id='{$commentID}' AND user_id='{$userID}'";
		$query = $this->db->query($sql);
		if ($query) {
			return true;
		} else {
			return null;
		}

	}
/////////////////////////////////////////////////////

	/**
	 * @param $title
	 * @param $content
	 * @param $catid
	 * @param $subcatid
	 */
	public function insertNewsdata($title, $content, $catid, $subcatid) {

		$query = "INSERT INTO $this->tbl (title, content, cat_id, subcat_id) VALUES ('" . $title . "','" . $content . "','" . $catid . "','" . $subcatid . "')";
		$this->db->query($query);

	}

	/**
	 * @param $idField
	 * @param $id
	 * @param $field
	 * @return mixed
	 */
	public function getField($idField, $id, $field) {

		$sql = "SELECT $field FROM $this->tbl WHERE $idField ='" . $id . "'";
		$query = $this->db->query($sql);
		if ($query->num_rows()) {
			$name = $query->result_array();
			$name = $name[0][$field];
			return $name;
		} else {
			return false;
		}

	}

	/**
	 * @param $catid
	 * @param $subcatid
	 * @param $field
	 * @return mixed
	 */
	public function getSub($catid, $subcatid, $field) {
		$query = "SELECT $field FROM $this->tbl WHERE catID ='" . $catid . "' AND id='" . $subcatid . "'";
		$result = $this->db->query($query);
		$name = $result->result_array();
		$name = $name[0][$field];
		return $name;
	}

	/**
	 * @return mixed
	 */
	public function getDataAll() {
		$query = $this->db->get($this->tblName);
		return $query->result_array();
	}

	/**
	 * @return mixed
	 */
	public function getDataAllMenues() {
		$query = $this->db->get($this->tblName);
		foreach ($query->result_array() as $item) {
			$data[$item['catEN']] = $item;
		}
		return $data;
	}

	public function getDataAllSubCats() {
		$query = $this->db->get($this->tblName);
		foreach ($query->result_array() as $item) {
			$data[$item['catID']][$item['subcatEN']] = $item;
		}
		return $data;
	}

	/**
	 * @param $catID
	 * @return mixed
	 */
	public function getDataAllSubmenues($catID) {
		$sql = "SELECT * FROM $this->tblName WHERE catID = '" . $catID . "'";
		$query = $this->db->query($sql);

		foreach ($query->result_array() as $item) {
			$data[$item['subcatEN']] = $item;
		}
		return $data;
	}

	/**
	 * @return mixed
	 */
	public function getNewsDataAll() {
		$this->db->where('published', '1');
		$this->db->order_by('created_at', 'DESC');
		$query = $this->db->get($this->tblName);
		return $query->result_array();
	}

	/**
	 * @param $limit
	 * @param $offset
	 * @return mixed
	 */
	public function getDataByLimit($limit = 4, $offset = 0) {
		$this->db->limit($limit, $offset);
		$this->db->order_by('created_at', 'DESC');
		$query = $this->db->get($this->tblName);
		return $query->result_array();
	}

	/**
	 * @param $limit
	 * @param $offset
	 * @return mixed
	 */
	public function getNewsLimitOne($limit = 1, $offset = 0) {
		$query = "SELECT * FROM $this->tblName WHERE published = '1' ORDER BY created_at DESC LIMIT $limit ";
		$query = $this->db->query($query);
		//print_r($query->result_array());exit;
		return $query->result_array();
	}

	/**
	 * @return mixed
	 */
	public function getDataAllOneCon() {
		$query = $this->db->get($this->tblName);
		return $query->result_array();
	}

	/**
	 * @return mixed
	 */
	public function getDataAllLimited() {
		$query = $this->db->get($this->tblName);
		return $query->result_array($this->limit);
	}

	/**
	 * @param $col
	 * @param $colVal
	 * @return mixed
	 */
	public function getDataOneCol($col, $colVal) {
		$this->db->where($col, $colVal);
		$query = $this->db->get($this->tblName);
		if ($query->num_rows()) {
			return $query->result_array()[0];
		} else {
			return false;
		}

	}

	/**
	 * @param $col
	 * @param $colVal
	 * @return mixed
	 */
	public function getDataOneColLimited($col, $colVal) {
		$this->db->where($col, $colVal);
		$query = $this->db->get($this->tblName);
		return $query->result_array($this->limit);
	}

	///////////////////////////////////////////////////////////

}
