<?php

class Persons_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->tbl = 'persons';
    }

    public function getInfo(int $id)
    {
        /**
        Load One info from database by ID
         **/
        $query = $this->db->where("id", $id)->get($this->tbl);
        $result = $query->result_array();

        if ($query->result_id->num_rows) {
            return $query->result_array()[0];
        } else {
            return null;
        }
    }

    public function saveInfo($user)
    {
        /**
        Get Info array and save to Persons database
         **/
        $this->tbl = "persons";
        $query = $this->db->where("id", $user['id'])->update(
            $this->tbl, $user
        );
        if($query) 
            return true;
        else
            return false;
    }

    public function createPerson($user){
        $blob = null;
        $nick = $user['firstName']." ".$user['lastName'];
        $sql = "INSERT INTO persons(id,fname,lname,email,nick,image) 
        VALUES('{$user['id']}','{$user['firstName']}','{$user['lastName']}','{$user['email']}',
        '{$nick}','{$blob}')";
        $this->db->query($sql);
    }

    public function getFields()
    {
        $sql = "SELECT * FROM {$this->tbl} limit 0,1";
        $query = $this->db->query($sql);
        $result = $query->result_array()[0];
        $fields = array();

        if ($query->result_id->num_rows > 0) {
            foreach ($result as $Key => $value) {
                $fields[] = $Key;
            }
            return $fields;
        } else {
            return null;
        }
    }

}
