<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function contentShorten(&$string)
{
    //print_r($string);
    foreach ($string as $key => $tm) {

        foreach ($string[$key] as $key2 => $d) {
            //print_r($d['content']);
            $limit = mb_strlen($string[$key][$key2]['title']);
            $string[$key][$key2]['content'] = mb_substr($string[$key][$key2]['content'], 0, 101 - $limit) . "...";
            //print_r("<br>".$string[$key][$key2]['content']);exit;
        }
        //print_r($string[$key][$key2]['content']);exit;
    }
    //print_r($string);exit;
    return $string;
}

function contentShortenc(&$string)
{
    //print_r($string);exit;
    foreach ($string as $key => $d) {
        //print_r($d['content']);
        $limit = mb_strlen($string[$key]['title']);
        $string[$key]['content'] = mb_substr($string[$key]['content'], 0, 200 - $limit) . "...";
        //print_r("<br>".$string[$key][$key2]['content']);exit;
    }
//print_r($string);exit;
    //print_r($string);exit;
    return $string;
}

function getThisMenuInfo($menulist, $catname)
{

    foreach ($menulist as $menu) {
        if ($menu['catEN'] == $catname) {
            return $menu;
        }
    }

}

function find_subcat_by_id($id, $subcats)
{
    foreach ($subcats as $subcat) {
        switch ($id) {
            case $id < 100000:return $subcat[0]['subcatEN'];
            case $id < 200000:return $subcat[1]['subcatEN'];
            case $id < 300000:return $subcat[2]['subcatEN'];
            case $id < 400000:return $subcat[3]['subcatEN'];
            case $id < 500000:return $subcat[4]['subcatEN'];
            case $id < 600000:return $subcat[5]['subcatEN'];
            case $id < 700000:return $subcat[6]['subcatEN'];
            case $id < 800000:return $subcat[7]['subcatEN'];
            case $id < 900000:return $subcat[8]['subcatEN'];
            case $id < 1000000:return $subcat[9]['subcatEN'];
            case $id < 1100000:return $subcat[10]['subcatEN'];
            case $id < 1200000:return $subcat[11]['subcatEN'];
            case $id < 1300000:return $subcat[12]['subcatEN'];
            case $id < 140000:return $subcat[13]['subcatEN'];
            case $id < 150000:return $subcat[14]['subcatEN'];
        }
    }

}

function sanitizeSting($var = '')
{
    return htmlentities(
        strip_tags(
            stripslashes($var)
        )
    );
}
